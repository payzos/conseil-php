<?php

namespace ConseilPHP;

class Transactions
{

    private $api;
    private $network = "mainnet";
    private $entity = "operations";

    public function __construct($_api)
    {
        $this->api = $_api;
    }

    /**
     * TODO
     * [ ] make an ability to check every kind transactions (for now just support single transaction for special purpose )
     * 
     * check tezos transaction with a hash_id, amount and start time.
     *
     * @param string    $_hash_id
     * @param int       $_amount
     * @param int       $_start_time
     * @return boolean|array [
     *  - source,
     *  - timestamp,
     *  - operation_group_hash,
     *  - amount,
     *  - fee
     * ]
     */
    public function check_transaction_status(string $_hash_id, int $_amount, int $_start_time)
    {
        $query_object = $this->make_check_transaction_request_body($_hash_id, $_amount, $_start_time);
        if (!is_array($query_object) || !$query_object) {
            return false;
        }

        $result = $this->api->send_request($this->network, $this->entity, $query_object);
        if (!$result) {
            return false;
        }
        if (empty($result) || !isset($result[0])) {
            return false;
        }
        if (!isset($result[0]["timestamp"]) || !isset($result[0]["source"]) || !isset($result[0]["operation_group_hash"])  || !isset($result[0]["fee"])) {
            return false;
        }
        $result[0]["timestamp"] = $result[0]["timestamp"] / 1000;
        return $result[0];
    }



    /**
     * TODO
     * [ ] make more ability
     * 
     * Make request body to check a transaction
     * we just make this Ts file to PHP :
     * https://github.com/Cryptonomic/ConseilJS/blob/master/src/reporting/ConseilQueryBuilder.ts
     * @param string $_destination_hash
     * @param int $_amount
     * @param int $_start_time
     * @return array
     */
    private function make_check_transaction_request_body(string $_destination_hash, int $_amount, int $_start_time)
    {
        $array = [
            'fields' => [
                "source",
                "timestamp",
                "operation_group_hash",
                "amount",
                "destination",
                "fee"
            ],
            'predicates' => [
                [
                    'field' => 'kind',
                    'operation' => 'eq',
                    'set' => ['transaction'],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => "destination",
                    'operation' => 'eq',
                    'set' => [$_destination_hash],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => 'timestamp',
                    'operation' => 'between',
                    'set' => [
                        $_start_time * 1000,
                        time() * 1000
                    ],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => 'amount',
                    'operation' => 'eq',
                    'set' => [
                        $_amount,
                    ],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => 'status',
                    'operation' => 'eq',
                    'set' => ['applied'],
                    'inverse' => false,
                    'group' => null
                ],
            ],
            'orderBy' => [
                [
                    'field' => 'timestamp',
                    'direction' => 'desc'
                ]
            ],
            'aggregation' => [],
            'limit' => 1

        ];
        return $array;
    }
}
