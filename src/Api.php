<?php

namespace ConseilPHP;

use GuzzleHttp\Client;

class Api
{

    /**
     * @var [type]
     */
    private $conseil_api_url;
    /**
     * @var [type]
     */
    private $conseil_platform;
    /**
     * @var [type]
     */
    private $conseil_api_key;


    /**
     * @param mixed $_conseil_api_url
     * @param mixed $_conseil_platform
     * @param mixed $_conseil_api_key
     */
    public function __construct($_conseil_api_url, $_conseil_platform, $_conseil_api_key)
    {
        $this->conseil_api_url  = $_conseil_api_url;
        $this->conseil_platform = $_conseil_platform;
        $this->conseil_api_key  = $_conseil_api_key;
    }

    /**
     * Undocumented function
     *
     * @param string $_network
     * @param string $_entity
     * @param array $_request_body
     * @return boolean|array
     */
    public function send_request(string $_network = "", string $_entity = "", array $_request_body = [])
    {
        $network = (!$_network == "") ? "/" . $_network : "";
        $entity = (!$_entity == "") ? "/" . $_entity : "";
        $url = $this->conseil_api_url . "/v2/data/" . $this->conseil_platform . $network . $entity;
        $client = new Client();
        $response = $client->post($url, [
            'json'        => $_request_body,
            'headers'     => [
                'Content-Type' => 'application/json',
                'apiKey' => $this->conseil_api_key,
            ],
        ]);

        if ($response->getStatusCode() != 200) {
            return false;
        }
        $array = json_decode($response->getBody(), true);
        if (!is_array($array)) {
            return false;
        }
        return $array;
    }
}
