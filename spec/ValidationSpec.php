<?php

namespace spec\ConseilPHP;

use ConseilPHP\Api;
use ConseilPHP\Validation;
use PhpSpec\ObjectBehavior;

class ValidationSpec extends ObjectBehavior
{
    public function it_is_work()
    {
        $api = new Api("https://conseil-prod.cryptonomic-infra.tech", "tezos", "9258f47b-4b9a-493d-977e-44a9b8ee5876");
        $validation = new Validation($api);
        return ($validation->wallet_hash("tz1a3T5mRR1XWM1JgydvAqxEwpNs7ktVZuXc"));
    }

    public function it_is_not_work()
    {
        $api = new Api("https://conseil-prod.cryptonomic-infra.tech", "tezos", "9258f47b-4b9a-493d-977e-44a9b8ee5876");
        $validation = new Validation($api);
        return ($validation->wallet_hash("tz1a3T5mRR1XWM1JgydvAqxEwpNs7ktVZu"));
    }
}
