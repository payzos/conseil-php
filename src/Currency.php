<?php

namespace ConseilPHP;

use GuzzleHttp\Client;

class Currency
{

    private $fixer_api_key;
    public function __construct($_fixer_api_key)
    {
        $this->fixer_api_key = $_fixer_api_key;
    }

    public function currency_to_usd(float $_value, string $_type)
    {
        if (strtoupper($_type) == "USD") {
            return intval($_value * 1000000);;
        }
        $amount = intval($_value * 1000000);
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $response = $client->get('https://data.fixer.io/api/convert', [
            'query' => [
                'access_key' => $this->fixer_api_key,
                'from' => $_type,
                'to' => 'USD',
                'amount' => $amount
            ]
        ]);
        if ($response->getStatusCode() != 200) {
            return false;
        }
        $fixer_array = json_decode($response->getBody(), true);
        if (!is_array($fixer_array)) {
            return false;
        }
        if (!isset($fixer_array["result"]) || !is_numeric($fixer_array["result"])) {
            return false;
        }
        $usd_amount = $fixer_array["result"];
        return $usd_amount;
    }


    public function xtz_price_in_usd()
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $response = $client->request('GET', 'https://api.coincap.io/v2/assets/tezos');
        if ($response->getStatusCode() != 200) {
            return false;
        }
        $cmk_array = json_decode($response->getBody(), true);
        if (!is_array($cmk_array)) {
            return false;
        }
        if (!isset($cmk_array["data"]["priceUsd"]) || !is_numeric($cmk_array["data"]["priceUsd"])) {
            return false;
        }
        return $cmk_array["data"]["priceUsd"];
    }

    public function currency_to_xtz(float $_value, string $_type)
    {
        $usd_amount = $this->currency_to_usd($_value, $_type);
        $xtz_price_in_usd = $this->xtz_price_in_usd();
        $cmk_amount = intval($xtz_price_in_usd * 1000000);
        $xtz_amount = intval(($usd_amount / $cmk_amount)  * 1000000);
        return $xtz_amount;
    }
}
