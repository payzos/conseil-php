<?php

namespace ConseilPHP;

/**
 * Conseil is a blockchain indexer & API for building decentralized applications, currently focused on Tezos.
 * - [Conseil](https://github.com/Cryptonomic/Conseil/)
 * - [API document](https://github.com/Cryptonomic/Conseil/tree/master/docs)
 * - [JavaScript client](https://github.com/Cryptonomic/ConseilJS)
 * - [JavaScript client document](https://cryptonomic.github.io/ConseilJS/)
 *
 * in this class we are using Conseil openApi to check validation of a payment under Tezos Network.
 *
 */
class Conseil
{

    /**
     * @var [type]
     */
    private $api;
    /**
     * @var [type]
     */
    private $transactions;
    /**
     * @var [type]
     */
    private $validation;
    /**
     * @var [type]
     */
    private $currency;


    /**
     * @param mixed $_conseil_api_url
     * @param mixed $_conseil_platform
     * @param mixed $_conseil_api_key
     */
    public function __construct($_conseil_api_url, $_conseil_platform, $_conseil_api_key, $_fixer_api_key)
    {
        $this->api = new Api($_conseil_api_url, $_conseil_platform, $_conseil_api_key);
        $this->transactions = new Transactions($this->api);
        $this->validation = new Validation($this->api);
        $this->currency = new Currency($_fixer_api_key);
    }

    /**
     * check tezos transaction with a hash_id, amount and start time.
     *
     * @param string    $_hash_id
     * @param int       $_amount
     * @param int       $_start_time
     * @return boolean|array [
     *  - source,
     *  - timestamp,
     *  - operation_group_hash,
     *  - amount,
     *  - fee
     * ]
     */
    public function get_payment_detail(string $_hash_id, int $_amount, int $_start_time)
    {
        return $this->transactions->check_transaction_status($_hash_id, $_amount, $_start_time);
    }

    /**
     * check is user input hash a valid tezos wallet hash ?
     *
     * @param string $_wallet_hash
     * @return boolean
     */
    public function validate_wallet_hash(string $_wallet_hash)
    {
        return $this->validation->wallet_hash($_wallet_hash);
    }

    /**
     * take your total and currency and will give you xtz total
     * @param int $_value
     * @param string $_type
     * 
     * @return boolean|int
     */
    public function get_xtz_total(float $_value, string $_type)
    {
        return $this->currency->currency_to_xtz($_value, $_type);
    }
}
