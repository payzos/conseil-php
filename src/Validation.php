<?php

namespace ConseilPHP;

/**
 * [Description Validation]
 */
class Validation
{
    /**
     * @var [type]
     */
    private $api;
    /**
     * @var string
     */
    private $network = "mainnet";
    /**
     * @var string
     */
    private $entity = "accounts";

    /**
     * @param mixed $_api
     */
    public function __construct($_api)
    {
        $this->api = $_api;
    }

    /**
     * check is user input hash a valid tezos wallet hash ?
     *
     * @param string $_wallet_hash
     * @return boolean
     */
    public function wallet_hash(string $_wallet_hash)
    {
        $public_key = $this->sanitize($_wallet_hash);
        if (!$public_key) {
            return false;
        }
        $query_object = $this->make_validation_query_body($public_key);
        if (!is_array($query_object) || !$query_object) {
            return false;
        }

        $result = $this->api->send_request($this->network, $this->entity, $query_object);
        if (!$result) {
            return false;
        }
        if (empty($result) || !isset($result[0])) {
            return false;
        }
        return true;
    }

    /**
     * @param string $_wallet_hash
     *
     * @return boolean|string
     */
    private function sanitize(string $_wallet_hash)
    {
        if ($_wallet_hash[0] !== 't' || $_wallet_hash[1] !== 'z') {
            return false;
        }
        return $_wallet_hash;
    }

    /**
     * @param string $_wallet_hash
     *
     * @return [type]
     */
    private function make_validation_query_body(string $_wallet_hash)
    {
        $array = [
            'fields' => [],
            'predicates' => [
                [
                    'field' => 'account_id',
                    'operation' => 'eq',
                    'set' => [$_wallet_hash],
                    'inverse' => false,
                    'group' => null,
                ],
            ],
            'orderBy' => [],
            'aggregation' => [],
            'limit' => 1,
        ];
        return $array;
    }
}
